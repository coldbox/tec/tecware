//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V2
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        Variable Handler
// Filename:      VARH_VariableHandler.h
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------

#ifndef VARH_VARIABLEHANDLER_H
#define VARH_VARIABLEHANDLER_H

#ifdef __cplusplus
extern "C" {
#endif

//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files (visible by all modules).
//=================================================================================================

#include "../SDEF_StandardDefinitions.h"

//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================

// Flags for variable
#define VARH_FLAG_VALID         (1<<0)  // is data valid
#define VARH_FLAG_OUTOFRANGE    (1<<1)  // is data out of range
#define VARH_FLAG_MODIFIED      (1<<2)  // is variable modified (compared to flash)
#define VARH_FLAG_FLASHCORRUPT  (1<<3)  // data from flash is corrupted

// Flags for variable infos
#define VARH_FLAGINFO_NONE      0       // no flag
#define VARH_FLAGINFO_FLOAT     (1<<0)  // variable in floating point format
#define VARH_FLAGINFO_SIGNED    (1<<1)  // variable is signed integer
#define VARH_FLAGINFO_BOOL      (1<<2)  // variable is boolean
#define VARH_FLAGINFO_READONLY  (1<<3)  // variable is readonly, master can not set variable
#define VARH_FLAGINFO_FLASH     (1<<4)  // variable is stored in flash

//=================================================================================================
// Section:       MACROS
// Description:   Definition of global macros (visible by all modules).
//=================================================================================================

//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of global enumerations (visible by all modules).
//=================================================================================================

// variables
typedef enum
{ 
  VARH_eMode = 0,

  VARH_eControlVoltage = 1,

  VARH_ePID_kp,
  VARH_ePID_ki,
  VARH_ePID_kd,
  VARH_ePID_Temp,
  VARH_ePID_Max,
  VARH_ePID_Min,

	VARH_eTemp_Water,
  VARH_eTemp_Module,
  VARH_eTemp_Diff,

  VARH_ePeltier_U,
  VARH_ePeltier_I,
  VARH_ePeltier_R,
  VARH_ePeltier_P,

  VARH_eSupply_U,
  VARH_eSupply_I,
  VARH_eSupply_P,

  VARH_ePowerState,
  VARH_eError,
  VARH_eRef_U,

  VARH_eNumberOfVariables,   // Must be last entry
} VARH_EnVariables;

//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of global Structures (visible by all modules).
//=================================================================================================

#pragma pack(4)
typedef union {
   FLOAT  flVal;
   U32    u32Val;
   S32    s32Val; 
} VARH_UVariable; 
#pragma pack()

typedef struct
{
  U8                        u8Flags;               // flags
  VARH_UVariable            uInitData;             // initial Data (data is always 32 bit)
  VARH_UVariable            uMinData;              // min Value for Data
  VARH_UVariable            uMaxData;              // max Value for Data
} VARH_StVarInfo;

typedef struct
{
  VARH_UVariable            uData;             	   // Data (data is always 32 bit)
  U8                        u8Flags;               // flags
} VARH_StVar;

//=================================================================================================
// Section:       GLOBAL VARIABLES
// Description:   Definition of global variables (visible by all modules).
//=================================================================================================

//=================================================================================================
// Section:       GLOBAL CONSTANTS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================

//=================================================================================================
// Section:       FUNCTION TYPES
// Description:   Definition of functions
//=================================================================================================

//=================================================================================================
// Section:       GLOBAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of global functions (visible by all modules).
//=================================================================================================

BOOL VARH_boInitializeModule( VOID );

// set data
VOID VARH_vSetVariableData( U8 u8Variable, VARH_UVariable uData );
VOID VARH_vSetVariableDataU32( U8 u8Variable, U32 u32Data );
VOID VARH_vSetVariableDataS32( U8 u8Variable, S32 s32Data );
VOID VARH_vSetVariableDataFloat( U8 u8Variable, FLOAT flData );

// set data from system
VOID VARH_vSetVariableDataFromSystem( U8 u8Variable, VARH_UVariable uData );
VOID VARH_vSetVariableDataFromSystemU32( U8 u8Variable, U32 u32Data );
VOID VARH_vSetVariableDataFromSystemS32( U8 u8Variable, S32 s32Data );
VOID VARH_vSetVariableDataFromSystemFloat( U8 u8Variable, FLOAT flData );

// get data
VARH_UVariable VARH_uGetVariableData( U8 u8Variable );
U32 VARH_u32GetVariableData( U8 u8Variable );
S32 VARH_s32GetVariableData( U8 u8Variable );
FLOAT VARH_flGetVariableData( U8 u8Variable );
U8 VARH_uGetVariableFlags( U8 u8Variable );

// reset data
VOID VARH_vSetVariableToInitData( U8 u8Variable );
VOID VARH_vSetAllVariablesToInitData( VOID );

BOOL VARH_vSaveVariablestoFlash( VOID );
VOID VARH_vLoadVariablesfromFlash( VOID );

#ifdef __cplusplus
}
#endif

#endif
