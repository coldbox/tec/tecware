//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V2
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        INIT_Initialization
// Filename:      INIT_Initialization.c
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------
//
// Description:   Initialization module
//
//=================================================================================================



//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files.
//=================================================================================================

#include "../SDEF_StandardDefinitions.h"
#include "INIT_Initialization.h"

// Application
#include "VARH_VariableHandler.h"
#include "PECO_PeltierController.h"
#include "MAIN_MainApplication.h"

// Drivers
#include "../Drivers/USFL_UserFlash.h"
#include "../Drivers/ANPI_AnalogPortsIn.h"
#include "../Drivers/ANPO_AnalogPortsOut.h"
#include "../Drivers/SPID_SpiDriver.h"
#include "../Drivers/DIPO_DigitalPorts.h"
#include "../Drivers/ADCD_AdcDriver.h"
#include "../Drivers/TEMP_Temperature.h"
#include "../Drivers/CAND_CanDriver.h"
#include "../Drivers/ERRH_ErrorHandler.h"

// Toolbox
#include "../Toolbox/UTIL_Utility.h"

#include "cmsis_os2.h" 

//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================


//=================================================================================================
// Section:       MACROS
// Description:   Definition of local macros (visible by this module only).
//=================================================================================================


//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of local enumerations (visible by this module only).
//=================================================================================================


//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of local Structures (visible by this module only).
//=================================================================================================


//=================================================================================================
// Section:       LOCAL VARIABLES
// Description:   Definition of local variables (visible by this module only).
//=================================================================================================
      
LOCAL osThreadId_t      m_pstThreadID     = NULL;

//=================================================================================================
// Section:       LOCAL CONSTANTS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================


LOCAL CONST osThreadAttr_t stTaskAttribute =
{ 
  "INIT_Thread",                    // name of the thread
  osThreadDetached,                 // attribute bits
  NULL,                             // memory for control block
  0,                                // size of provided memory for control block
  NULL,                             // memory for stack
  1024,                             // size of stack
  osPriorityHigh7,                  // initial thread priority (default: osPriorityNormal)
  0,                                // TrustZone module identifier
  0,                                // reserved (must be 0)
};

//=================================================================================================
// Section:       LOCAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of local functions (visible by this module only).
//=================================================================================================

PRIVATE VOID vTask ( PVOID arg )__NORETURN;

//=================================================================================================
// Section:       EXTERNAL FUNCTIONS
// Description:   Definition of external (global) functions.
//=================================================================================================



//=================================================================================================
// Section:       EXTERNAL VARIABLES
// Description:   Definition of external (global) variables.
//=================================================================================================


//=================================================================================================
// Section:       GLOBAL FUNCTIONS
// Description:   Definition (implementation) of global functions.
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// Function:      INIT_boCreateTask
// Description:   Create the init Task
// Parameters:    None
// Returns:       Boolean TRUE if successful
//-------------------------------------------------------------------------------------------------
BOOL INIT_boCreateTask( VOID )
{
  BOOL boOK = TRUE;
  
  boOK &= ((m_pstThreadID = osThreadNew( vTask, NULL, &stTaskAttribute )) != NULL);   // create init Task
  
  return( boOK );
}


//-------------------------------------------------------------------------------------------------
// Function:      vInitTask
// Description:   Initialization Task, priority must be high!
// Parameters:    None
// Returns:       None
//-------------------------------------------------------------------------------------------------
PRIVATE VOID vTask( PVOID arg )
{
	UNUSED( arg );

	BOOL boOK         = TRUE;

	uint32_t reset_flags = RCC->CSR;

	boOK &= ERRH_boInitializeModule();
	boOK &= USFL_boInitializeModule();
	boOK &= VARH_boInitializeModule();

	if (reset_flags & RCC_CSR_IWDGRSTF) {
	  // This was IWDG
	  ERRH_vSetError(HARDFAULT_ERROR_MASK | HARDFAULT_ERROR_IWDG);
	}else if (reset_flags & RCC_CSR_SFTRSTF) {
	  // Software reset (NVIC_SystemReset or by debugger)
	  ERRH_vSetError(HARDFAULT_ERROR_MASK | HARDFAULT_ERROR_SWRST);
	}

	boOK &= DIPO_boInitializeModule();
	boOK &= ANPI_boInitializeModule();
	boOK &= ANPO_boInitializeModule();
	boOK &= SPID_boInitializeModule();
	boOK &= ADCD_boInitializeModule();
	boOK &= TEMP_boInitializeModule();
	boOK &= CAND_boInitializeModule();

	boOK &= PECO_boInitializeModule();
	boOK &= MAIN_boInitializeModule();



  if( !boOK ){
    osKernelLock(); // lock kernel to prevent task switch
    PECO_Enable( FALSE );
    while( 1 ){ // Toggle Error LED fast
      DIPO_vToggleOutput( DIPO_eLED );
      DELAY_MS( 100 );
    }
  }

  osThreadSuspend( m_pstThreadID );
  while( 1 );
}

//=================================================================================================
// Section:       LOCAL FUNCTIONS
// Descriptionn:  Definition (implementation) of local functions.
//=================================================================================================

