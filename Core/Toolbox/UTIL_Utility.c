//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V3
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        Utility
// Filename:      UTIL_Utility.c
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------
//
// Description:   This source file contains a few utility functions
//
//=================================================================================================



//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files.
//=================================================================================================

#include "UTIL_Utility.h"


//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================

// defines the registers for the debug printf via trace
#define ITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define ITM_Port16(n)   (*((volatile unsigned short*)(0xE0000000+4*n)))
#define ITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))
#define DEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define TRCENA          0x01000000

#define DBGMCU_IDCOE    0xE0042000

//=================================================================================================
// Section:       MACROS
// Description:  Definition of local macros (visible by this module only).
//=================================================================================================



//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of local enumerations (visible by this module only).
//=================================================================================================




//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of local Structures (visible by this module only).
//=================================================================================================



//=================================================================================================
// Section:       LOCAL VARIABLES
// Description:   Definition of local variables (visible by this module only).
//=================================================================================================



//=================================================================================================
// Section:       LOCAL CONSTANTS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================

//=================================================================================================
// Section:       LOCAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of local functions (visible by this module only).
//=================================================================================================


//=================================================================================================
// Section:       EXTERNAL FUNCTIONS
// Description:   Definition of external (global) functions.
//=================================================================================================



//=================================================================================================
// Section:       EXTERNAL VARIABLES
// Description:   Definition of external (global) variables.
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL FUNCTIONS
// Description:   Definition (implementation) of global functions.
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// Function:      UTIL_u16StringLength
// Description:   Returns the length of the string without zero termination
// Parameters:    PSZ pszString
// Returns:       U16, string length
//-------------------------------------------------------------------------------------------------
U16 UTIL_u16StringLength( PSZ pszString )
{
  U16 u16Cnt = 0;
  
  while( *pszString++ != '\0' )
  {
    u16Cnt++;
  }
  return( u16Cnt ); 
}

//-------------------------------------------------------------------------------------------------
// Function:      UTIL_u16CheckStringLength
// Description:   Returns TRUE, if the string leght (withouth zero termination) is less or equal the u16MaxLength
// Parameters:    PSZ pszString
//                U16 u16MaxLength
// Returns:       U16, string length
//-------------------------------------------------------------------------------------------------
BOOL UTIL_boCheckStringLength( PSZ pszString, U16 u16MaxLength )
{
  while( *pszString++ != '\0' )
  {
    if( u16MaxLength == 0 )
    {
      return( FALSE );
    }
    u16MaxLength--;
  }
  return( TRUE ); 
}

//-------------------------------------------------------------------------------------------------
// Function:      UTIL_vPuts
// Description:   Prints a string
// Parameters:    PSZ pszString
// Returns:       None
//-------------------------------------------------------------------------------------------------
VOID  UTIL_vPuts(  PSZ pszString )
{
  while( *pszString != '\0' )
  {
    putchar( *pszString++ );
  }
}

//-------------------------------------------------------------------------------------------------
// Function:    UTIL_u8FindMajority
// Description: Finds the majority element of a given data set
// Parameters:  PU8 pu8Data
//              U16 u16Length
// Returns:     U8
//-------------------------------------------------------------------------------------------------
U8 UTIL_u8FindMajority( PU8 pu8Data, U16 u16Length )
{
  U32 u32MaxCount = 0;  
  S32 s32Index = -1; 
  
  for( U32 i = 0; i < u16Length; i++) 
  { 
      U32 u32Count = 0; 
      for( int j = 0; j < u16Length; j++ ) 
      { 
          if(pu8Data[i] == pu8Data[j]) 
          u32Count++; 
      } 
        
      // update maxCount if count of  
      // current element is greater 
      if( u32Count > u32MaxCount ) 
      { 
        u32MaxCount = u32Count; 
        s32Index = i; 
      } 
  } 
    
  // if maxCount is greater than n/2  
  // return the corresponding element  
  if( u32MaxCount > u16Length/2 && s32Index >= 0 )
  {
    return( pu8Data[s32Index] );
  }    
  else
  {
    return( 0 ); 
  }

}

//-------------------------------------------------------------------------------------------------
// Function:    UTIL_vMemCopy
// Description: Copies a set of data.
// Parameters:  PVOID pvSource  pointer to the source data
//              VOID pvDest     pointer destination data
//              U16 u16Length   number of bytes to copy
// Returns:     None
//-------------------------------------------------------------------------------------------------
VOID  UTIL_vMemCopy( PVOID pvSource, PVOID pvDest, U16 u16Length )
{
  // use memcpy for better performance
  memcpy( pvDest, pvSource, u16Length );
}

//-------------------------------------------------------------------------------------------------
// Function:    UTIL_u32MemCopy
// Description: Copies a set of data.
// Parameters:  PVOID pvSource                pointer to the source data
//              PVOID pvDest                  pointer destination data
//              U16 u16Length                 number of bytes to copy
//              SDEF_EnByteOrder enByteOrder  byte order
// Returns:     U32 Number of Bytes copied
//-------------------------------------------------------------------------------------------------
U32 UTIL_u32MemCopy( PVOID pvSource, PVOID pvDest, U16 u16Length, SDEF_EnByteOrder enByteOrder )
{    
  if( enByteOrder == SDEF_eNormalByteOrder )
  {
    UTIL_vMemCopy( pvSource, pvDest, u16Length );
  }
  else
  {
    // copy the bytes in reverse order
    PU8 pu8Source = pvSource;
    PU8 pu8Dest = pvDest;
    for( U16 u16Cnt = 0; u16Cnt < (u16Length+1)/2; u16Cnt++ )
    {
      pu8Dest[u16Cnt] = pu8Source[u16Length - 1 - u16Cnt];
      pu8Dest[u16Length - 1 - u16Cnt] = pu8Source[u16Cnt];
    }
  }

  return( u16Length );
}

//-------------------------------------------------------------------------------------------------
// Function:    UTIL_vMemCopyU32
// Description: Copies a set of four byte data types.
// Parameters:  PU32 pu32Source pointer to the source data
//              PU32 pu32Dest   pointer destination data
//              U16 u16Length   number of bytes to copy
// Returns:     None
//-------------------------------------------------------------------------------------------------
VOID  UTIL_vMemCopyU32( PU32 pu32Source, PU32 pu32Dest, U16 u16Length )
{
  while( u16Length-- > 0 )
  {
    *pu32Dest++ = *pu32Source++;
  }
}

//-------------------------------------------------------------------------------------------------
// Function:    UTIL_boMemCompare
// Description: Compares a set of data to equality
// Parameters:  PU8 pu8Data1      pointer to first data set
//              PU8 pu8Data2      pointer to second data set
//              U32 u32Length     number of bytes to compare
// Returns:     BOOL              TRUE, if compared data is equal, otherwise FALSE
//-------------------------------------------------------------------------------------------------

BOOL UTIL_boMemCompare( PU8 pu8Data1, PU8 pu8Data2, U32 u32Length )
{
  while( u32Length-- > 0 )
  {
    if( *pu8Data1++ != *pu8Data2++ )
    {
      return( FALSE );
    }
  }
  return( TRUE );
}

//-------------------------------------------------------------------------------------------------
// Function:    UTIL_boMemCompareVal
// Description: Compares if the all the data has the value specifed with u8Value.
// Parameters:  PU8 pu8Data       pointer to data set
//              U8  u8Value       value to compare
//              U32 u32Length     number of bytes to compare
// Returns:     BOOL              TRUE, if compared data is equal, otherwise FALSE
//-------------------------------------------------------------------------------------------------

BOOL  UTIL_boMemCompareVal( PU8 pu8Data, U8 u8Value, U32 u32Length )
{
  while( u32Length-- > 0 )
  {
    if( *pu8Data != u8Value)
    {
      return( FALSE );
    }
    pu8Data++;
  }
  return( TRUE );
}

//-------------------------------------------------------------------------------------------------
// Function:      UTIL_u8DecToBCD
// Description:   Converts decimal encoded byte to bcd value
// Parameters:    decimal value to convert to bcd
// Returns:       None
//-------------------------------------------------------------------------------------------------
U8 UTIL_u8DecToBCD( U8 u8Value )
{
 return ( (u8Value/10*16) + (u8Value%10) );
}

//-------------------------------------------------------------------------------------------------
// Function:      UTIL_u8BCDToDec
// Description:   Converts bcd to decimal
// Parameters:    bcd value to convert to decimal
// Returns:       None
//-------------------------------------------------------------------------------------------------
U8 UTIL_u8BCDToDec( U8 u8Value )
{
  return ( (u8Value/16*10) + (u8Value%16) );
}

//-------------------------------------------------------------------------------------------------
// Function:      UTIL_vReverseBytes
// Description:   Reverses the byte order
// Parameters:    PU8 pu8Data           data
//                U16 u16NumberOfBytes  number of bytes
// Returns:       None
//-------------------------------------------------------------------------------------------------
VOID  UTIL_vReverseBytes( PU8 pu8Data, U16 u16NumberOfBytes )
{
  U8 u8Tmp;
  for( U16 u16Cnt = 0; u16Cnt < u16NumberOfBytes/2; u16Cnt++ )
  {
    u8Tmp = pu8Data[u16Cnt];
    pu8Data[u16Cnt] = pu8Data[u16NumberOfBytes - 1 - u16Cnt];
    pu8Data[u16NumberOfBytes - 1 - u16Cnt] = u8Tmp;
  }
}

//-------------------------------------------------------------------------------------------------
// Function:      UTIL_u32RevU32
// Description:   Reverses the bytes of an U32
// Parameters:    U32 u32Data
// Returns:       U32   reversed data
//-------------------------------------------------------------------------------------------------
U32 UTIL_u32RevU32( U32 u32Data )
{
  return( __REV(u32Data) );
}

//-------------------------------------------------------------------------------------------------
// Function:      UTIL_u32RevFLOAT
// Description:   Reverses the bytes of an FLOAT
// Parameters:    FLOAT flData
// Returns:       U32   reversed data
//-------------------------------------------------------------------------------------------------
U32 UTIL_u32RevFLOAT( FLOAT flData )
{
  U32 u32Data = *(PU32)&flData;
  return( __REV(u32Data) );
}

//-------------------------------------------------------------------------------------------------
// Function:      UTIL_u16RevU16
// Description:   Reverses the bytes of an U16
// Parameters:    U16 u16Data
// Returns:       U16   reversed data
//-------------------------------------------------------------------------------------------------
U16 UTIL_u16RevU16( U16 u16Data )
{
  return( (U16)__REV16(u16Data) );
}

//-------------------------------------------------------------------------------------------------
// Function:      UTIL_cGetDeviceRevision
// Description:   Gets the silicon revision of the STM32F437
// Parameters:    None
// Returns:       CHAR
//-------------------------------------------------------------------------------------------------
CHAR  UTIL_cGetDeviceRevision( VOID )
{
  U16 u16DeviceID = (*(PU32)DBGMCU_IDCOE) >> 16;
  
  switch( u16DeviceID )
  {
    case 0x100:
      return( 'A' );
    
    case 0x1001:
      return( 'Z' );
    
    case 0x1003:
      return( 'Y' );
    
    case 0x1007:
      return( '1' );
    
    case 0x2001:
      return( '3' );
    
    default:
      return( '0' );
  } 
}

//-------------------------------------------------------------------------------------------------
// Function:    fputc
// Description: Prototype for printf
//-------------------------------------------------------------------------------------------------
int fputc( int ch, FILE *f ) 
{
  UNUSED( f );    // file stream not used
  
  if( DEMCR & TRCENA ) 
  {
    while( ITM_Port32(0) == 0 );
    ITM_Port8(0) = (U8)ch;
  }
  return( ch );
}

//=================================================================================================
// Section:      LOCAL FUNCTIONS
// Description:  Definition (implementation) of local functions.
//=================================================================================================

