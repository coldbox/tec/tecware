//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V2
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        Can Driver
// Filename:      CAND_CanDriver.h
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------

#ifndef CAND_CANDRIVER_H
#define CAND_CANDRIVER_H

#ifdef __cplusplus
extern "C" {
#endif



//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files (visible by all modules).
//=================================================================================================

#include "../SDEF_StandardDefinitions.h"

//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================

#define MESSAGE_TYPE_WRITE          2
#define MESSAGE_TYPE_READ           1
#define MESSAGE_TYPE_COMMAND        0

//=================================================================================================
// Section:       MACROS
// Description:   Definition of global macros (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of global enumerations (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of global Structures (visible by all modules).
//=================================================================================================

typedef struct {
	U8 au8Data[8];
	U8 u8Len;
	U8 u8Type;
	BOOL boIsPrivate;
} CAND_Message;

// callback functions
typedef VOID (*CAND_pfnRxCallback)( CAND_Message stMessage );

//=================================================================================================
// Section:       GLOBAL VARIABLES
// Description:   Definition of global variables (visible by all modules).
//=================================================================================================


//=================================================================================================
// Section:       GLOBAL CONSTANTS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of global functions (visible by all modules).
//=================================================================================================

BOOL CAND_boInitializeModule( VOID );
BOOL CAND_boSendMessage( PU8 pu8Buffer, U8 u8Len, BOOL boIsPrivate, U8 u8Type );
VOID CAND_vSetRxCallback( CAND_pfnRxCallback pfnRxCallback );
U8 CAND_u8GetBoardId( VOID );

#ifdef __cplusplus
}
#endif

#endif
