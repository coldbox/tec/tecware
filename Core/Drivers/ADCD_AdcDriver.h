//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V2
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        ADC Driver
// Filename:      ADCD_AdcDriver.h
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------

#ifndef ADCD_ADCDRIVER_H
#define ADCD_ADCDRIVER_H

#ifdef __cplusplus
extern "C" {
#endif

//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files (visible by all modules).
//=================================================================================================

#include "../SDEF_StandardDefinitions.h"

//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================

#define ADCD_STATUS_DATA_ERROR 			0x01
#define ADCD_SPI_FAILURE				0x02

#define ADCD_FAULT_HIGHTHRESH 			0x80
#define ADCD_FAULT_LOWTHRESH 			0x40
#define ADCD_FAULT_REFINLOW 			0x20
#define ADCD_FAULT_REFINHIGH 			0x10
#define ADCD_FAULT_RTDINLOW 			0x08
#define ADCD_FAULT_OVUV 				0x04
#define ADCD_FAULT_MASK					0xFC

//=================================================================================================
// Section:       MACROS
// Description:   Definition of global macros (visible by all modules).
//=================================================================================================


//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of global enumerations (visible by all modules).
//=================================================================================================

typedef enum {
	ADCD_eWater = 0,
	ADCD_eModule = 1,
	
	ADCD_eNumberOfTemps  // Must be last
}ADCD_EnTemps;

//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of global Structures (visible by all modules).
//=================================================================================================


//=================================================================================================
// Section:       GLOBAL VARIABLES
// Description:   Definition of global variables (visible by all modules).
//=================================================================================================


//=================================================================================================
// Section:       GLOBAL CONSTANTS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================


//=================================================================================================
// Section:       GLOBAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of global functions (visible by all modules).
//=================================================================================================

BOOL    ADCD_boInitializeModule( VOID );
BOOL ADCD_boReadData( ADCD_EnTemps eChannel, PU8 pu8Error, PU16 pu16Data );

#ifdef __cplusplus
}
#endif

#endif
