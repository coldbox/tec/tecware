//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V2
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        Can Driver
// Filename:      CAND_CanDriver.c
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------
//
// Description:   This source file contains all functions dealing with the analog outputs ports
//
//=================================================================================================



//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files.
//=================================================================================================

#include "CAND_CanDriver.h"

// Toolbox
#include "../Toolbox/UTIL_Utility.h"

// Driver
#include "DIPO_DigitalPorts.h"

#include "ERRH_ErrorHandler.h"

// include STM32 drivers
#include "stm32l4xx_hal.h"

#include "cmsis_os2.h" 

//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================

#define CAND_SHIFT 0x1 // Shift address space (0-3)

#define CAND_PRIVATE 1
#define CAND_PUBLIC 0

#define CAND_RECEIVE 0
#define CAND_SEND 1

//=================================================================================================
// Section:       MACROS
// Description:   Definition of local macros (visible by this module only).
//=================================================================================================



//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of local enumerations (visible by this module only).
//=================================================================================================


//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of local Structures (visible by this module only).
//=================================================================================================


//=================================================================================================
// Section:       LOCAL VARIABLES
// Description:   Definition of local variables (visible by this module only).
//=================================================================================================
CAND_pfnRxCallback m_pfnRxCallback = NULL;

U8 u8BoardId = 0x00;

CAN_FilterTypeDef stFilter_public = {
	(CAND_SHIFT<<14)|(CAND_PUBLIC<<13)|(CAND_RECEIVE<<11), // FilterIdHigh
	0x0000, // FilterIdLow
	0xE800, // FilterMaskIdHigh
	0x0000, // FilterMaskIdLow
	CAN_FILTER_FIFO0, // FilterFIFOAssignment
	0x00, // FilterBank
	CAN_FILTERMODE_IDMASK, // FilterMode
	CAN_FILTERSCALE_32BIT, // FilterScale
	CAN_FILTER_ENABLE, // FilterActivation
	0x00 // SlaveStartFilterBank
};

CAN_FilterTypeDef stFilter_private = {
	(CAND_SHIFT<<14)|(CAND_PRIVATE<<13)|(CAND_RECEIVE<<11), // FilterIdHigh
	0x0000, // FilterIdLow
	0xE9E0, // FilterMaskIdHigh
	0x0000, // FilterMaskIdLow
	CAN_FILTER_FIFO0, // FilterFIFOAssignment
	0x01, // FilterBank
	CAN_FILTERMODE_IDMASK, // FilterMode
	CAN_FILTERSCALE_32BIT, // FilterScale
	CAN_FILTER_ENABLE, // FilterActivation
	0x00 // SlaveStartFilterBank
};

//=================================================================================================
// Section:       LOCAL CONSTANTS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================


//=================================================================================================
// Section:       LOCAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of local functions (visible by this module only).
//=================================================================================================


//=================================================================================================
// Section:       EXTERNAL FUNCTIONS
// Description:   Definition of external (global) functions.
//=================================================================================================



//=================================================================================================
// Section:       EXTERNAL VARIABLES
// Description:   Definition of external (global) variables.
//=================================================================================================

extern CAN_HandleTypeDef hcan1;

//=================================================================================================
// Section:       GLOBAL FUNCTIONS
// Description:   Definition (implementation) of global functions.
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// Function:      CAND_boInitializeModule
// Description:   Initializes the module. Function must be called once immediately after power-up.
// Parameters:    None
// Returns:       Boolean, TRUE if successful
//-------------------------------------------------------------------------------------------------
BOOL CAND_boInitializeModule( VOID )
{
	BOOL boOK = TRUE;
	
	u8BoardId |= DIPO_boGetInput(DIPO_eADR0) << 0;
	u8BoardId |= DIPO_boGetInput(DIPO_eADR1) << 1;
	u8BoardId |= DIPO_boGetInput(DIPO_eADR2) << 2;
	u8BoardId |= DIPO_boGetInput(DIPO_eADR3) << 3;

	stFilter_private.FilterIdHigh |= u8BoardId << 5;

	boOK &= ( HAL_CAN_ConfigFilter(&hcan1, &stFilter_public) == HAL_OK ) ? TRUE : FALSE;
	boOK &= ( HAL_CAN_ConfigFilter(&hcan1, &stFilter_private) == HAL_OK ) ? TRUE : FALSE;
	
	boOK &= ( HAL_CAN_Start(&hcan1) == HAL_OK ) ? TRUE : FALSE;
	
	boOK &= ( HAL_CAN_ActivateNotification( &hcan1, CAN_IT_RX_FIFO0_MSG_PENDING ) == HAL_OK ) ? TRUE : FALSE;

  return( boOK );
}

//-------------------------------------------------------------------------------------------------
// Function:      CAND_u8GetBoardId
// Description:   Returns the Board ID
// Parameters:    None
// Returns:       U8 Board ID
//-------------------------------------------------------------------------------------------------
U8 CAND_u8GetBoardId( VOID ){
	return u8BoardId;
}

//-------------------------------------------------------------------------------------------------
// Function:      CAND_boSendMessage
// Description:   Send a Message over the CAN Interface
// Parameters:    U8 Id
//								U8 Length
//								PU8 Data Buffer
// Returns:       Boolean, TRUE if successful
//-------------------------------------------------------------------------------------------------
BOOL CAND_boSendMessage( PU8 pu8Buffer, U8 u8Len, BOOL boIsPrivate, U8 u8Type ){
	BOOL boOK = TRUE;
	U16 u16Id = 0x000;
	
	if( u8Len > 8 ) return FALSE;

	u16Id = (CAND_SHIFT<<9)|(boIsPrivate<<8)|(CAND_SEND<<6)|(u8Type<<4)|(U16)u8BoardId;
	
	
	CAN_TxHeaderTypeDef header = {
		u16Id,
		0,
		CAN_ID_STD,
		CAN_RTR_DATA,
		u8Len,
		DISABLE
	};
	
	boOK &= HAL_CAN_AddTxMessage( &hcan1, &header, pu8Buffer, (PU32)CAN_TX_MAILBOX0 ) == HAL_OK ? TRUE : FALSE;
	
	return boOK;
}


//-------------------------------------------------------------------------------------------------
// Function:      CAND_vSetRxCallback
// Description:   Send a Message over the CAN Interface
// Parameters:    CAND_pfnRxCallback Callback Function
// Returns:       none
//-------------------------------------------------------------------------------------------------
VOID CAND_vSetRxCallback( CAND_pfnRxCallback pfnRxCallback ){
	m_pfnRxCallback = pfnRxCallback;
}



//=================================================================================================
// Section:       LOCAL FUNCTIONS
// Descriptionn:  Definition (implementation) of local functions.
//=================================================================================================


//-------------------------------------------------------------------------------------------------
// Function:      HAL_CAN_RxFifo0MsgPendingCallback
// Description:   HAL Can Rx FIFO Msg Pending Callback
// Parameters:    CAN_HandleTypeDef *hcan
// Returns:       None
//-------------------------------------------------------------------------------------------------
void HAL_CAN_RxFifo0MsgPendingCallback( CAN_HandleTypeDef *hcan ){
	
	CAN_RxHeaderTypeDef header;
	U8 au8Data[8];
	
	HAL_CAN_GetRxMessage( hcan, CAN_RX_FIFO0, &header, au8Data );
	
	CAND_Message stMessage;

	stMessage.boIsPrivate = (header.StdId & 0x100) >> 8;
	stMessage.u8Type = (header.StdId & 0x030) >> 4;	
	stMessage.u8Len = header.DLC;
	UTIL_vMemCopy( au8Data,stMessage.au8Data,stMessage.u8Len );
	
	m_pfnRxCallback( stMessage );
	
}

//-------------------------------------------------------------------------------------------------
// Function:      HAL_CAN_ErrorCallback
// Description:   HAL Can error callback function
// Parameters:    CAN_HandleTypeDef *hcan
// Returns:       None
//-------------------------------------------------------------------------------------------------
void HAL_CAN_ErrorCallback( CAN_HandleTypeDef *hcan ){
	U32 u32Error = hcan->ErrorCode;
	ERRH_vSetError(CAN_ERROR_MASK | u32Error);
}



