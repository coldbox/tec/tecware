//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V2
// Author:        Noah Piqu� (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        Digital Ports
// Filename:      DIPO_DigitalPorts.h
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------

#ifndef DIPO_DIGITALPORTS_H
#define DIPO_DIGITALPORTS_H

#ifdef __cplusplus
extern "C" {
#endif



//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files (visible by all modules).
//=================================================================================================

#include "../SDEF_StandardDefinitions.h"


//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       MACROS
// Description:   Definition of global macros (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of global enumerations (visible by all modules).
//=================================================================================================

// Enumeration for digital inputs is used as array index later on. Thus enumeration must start 
// at zero and must be numbered consecutively. Enumeration must never be changed.
// Value 0xFF is reserved and must not be used.

typedef enum
{
	
	DIPO_ePG								= 0,				// 00 DIP 1
	DIPO_eADR3								= 1,				// 01 DIP 2
	DIPO_eADR2								= 2,				// 01 DIP 2
	DIPO_eADR1								= 3,				// 02 DIP 3
	DIPO_eADR0								= 4,				// 03 DIP 4

  DIPO_eInNumberOfInputs,                   // Must be last entry     
} DIPO_EnDigitalInput;


// Enumeration for digital outputs is used as array index later on. Thus enumeration must start 
// at zero and must be numbered consecutively. Enumeration must never be changed.
// Value 0xFF is reserved and must not be used.

typedef enum
{

	DIPO_eCS_Module         					= 0,      	// 00 Chip Select Module
	DIPO_eCS_Water         						= 1,        // 01 Chip Select Water
	
	DIPO_eLED        							= 2,        // 02  Test LED
	DIPO_eEN        							= 3,        // 03 Enable


  DIPO_eOutNumberOfOutputs,                   	// Must be last entry
} DIPO_EnDigitalOutput;


//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of global Structures (visible by all modules).
//=================================================================================================




//=================================================================================================
// Section:       GLOBAL VARIABLES
// Description:   Definition of global variables (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL CONSTANTS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of global functions (visible by all modules).
//=================================================================================================

BOOL DIPO_boInitializeModule( VOID );
VOID DIPO_vSetOutput( DIPO_EnDigitalOutput enOutput );
VOID DIPO_vResetOutput( DIPO_EnDigitalOutput enOutput );
VOID DIPO_vSetState( DIPO_EnDigitalOutput enOutput, BOOL boState );

VOID DIPO_vToggleOutput( DIPO_EnDigitalOutput enOutput );
BOOL DIPO_boGetInput( DIPO_EnDigitalInput enInput );
BOOL DIPO_boGetOutput( DIPO_EnDigitalOutput enOutput );

#ifdef __cplusplus
}
#endif

#endif
