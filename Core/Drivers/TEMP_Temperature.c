//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V2
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        Temp
// Filename:      TEMP_Temperature.c
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------
//
// Description:   This source file contains all functions dealing with the temperature readout
//
//=================================================================================================

//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files.
//=================================================================================================

#include "TEMP_Temperature.h"

// Application
#include "../Application/VARH_VariableHandler.h"

// Toolbox
#include "../Toolbox/UTIL_Utility.h"

#include "ERRH_ErrorHandler.h"

// Drivers
#include "ADCD_AdcDriver.h"

// include STM32 drivers
#include "stm32l4xx_hal.h"

#include "cmsis_os2.h" 

//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================

#define REFRESH_MS 100

//=================================================================================================
// Section:       MACROS
// Description:   Definition of local macros (visible by this module only).
//=================================================================================================

//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of local enumerations (visible by this module only).
//=================================================================================================

//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of local Structures (visible by this module only).
//=================================================================================================

//=================================================================================================
// Section:       LOCAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of local functions (visible by this module only).
//=================================================================================================

PRIVATE FLOAT flConvertADCData( U16 dbRTemp );
PRIVATE VOID vTask( PVOID arg );

//=================================================================================================
// Section:       LOCAL CONSTANTS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================

LOCAL CONST osThreadAttr_t stTaskAttribute =
{
  "TEMP_Thread",                    // name of the thread
  osThreadDetached,                 // attribute bits
  NULL,                             // memory for control block
  0,                                // size of provided memory for control block
  NULL,                             // memory for stack
  1024,                             // size of stack
  osPriorityNormal,                 // initial thread priority (default: osPriorityNormal)
  0,                                // TrustZone module identifier
  0,                                // reserved (must be 0)
};

//=================================================================================================
// Section:       LOCAL VARIABLES
// Description:   Definition of local variables (visible by this module only).
//=================================================================================================

LOCAL osThreadId_t      m_pstThreadID   	= NULL;

//=================================================================================================
// Section:       EXTERNAL FUNCTIONS
// Description:   Definition of external (global) functions.
//=================================================================================================

//=================================================================================================
// Section:       EXTERNAL VARIABLES
// Description:   Definition of external (global) variables.
//=================================================================================================

//=================================================================================================
// Section:       GLOBAL FUNCTIONS
// Description:   Definition (implementation) of global functions.
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// Function:      TEMP_boInitializeModule
// Description:   Initializes the module. Function must be called once immediately after power-up.
//                This function is thread save
// Parameters:    None
// Returns:       Boolean TRUE if successful
//-------------------------------------------------------------------------------------------------
BOOL TEMP_boInitializeModule( VOID )
{ 
  BOOL boOK = TRUE;

  boOK &= ((m_pstThreadID = osThreadNew( vTask, NULL, &stTaskAttribute )) == NULL ) ? FALSE : TRUE;

  return( boOK );
}

//=================================================================================================
// Section:       LOCAL FUNCTIONS
// Descriptionn:  Definition (implementation) of local functions.
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// Function:      vTempTask
// Description:   vTempTask
// Parameters:    None
// Returns:       None
//-------------------------------------------------------------------------------------------------
PRIVATE VOID vTask( PVOID arg )
{
  UNUSED( arg );
	
	BOOL boOK = TRUE;
	U8 u8error = 0;
	U16 u16ADC_data;
	FLOAT flTempData[ADCD_eNumberOfTemps];
	
	osDelay(10);
  
  while( TRUE )
  {		
    boOK &= ADCD_boReadData( ADCD_eWater, &u8error, &u16ADC_data );

    if( boOK )
    {
      flTempData[ADCD_eWater] = flConvertADCData( u16ADC_data );
    } else {
        if( (u8error & ADCD_STATUS_DATA_ERROR) == ADCD_STATUS_DATA_ERROR ){
        	BOOL boFatal = TRUE;
        	if( (u8error & ADCD_FAULT_HIGHTHRESH) == ADCD_FAULT_HIGHTHRESH ){ // open
        		flTempData[ADCD_eWater] = 4200.0f;
        		boFatal &= FALSE;
        	}else if( (u8error & ADCD_FAULT_LOWTHRESH) == ADCD_FAULT_LOWTHRESH ){ // shorted
        		flTempData[ADCD_eWater] = -273.16f;
        		boFatal &= FALSE;
        	}else if( (u8error & ADCD_FAULT_REFINLOW) == ADCD_FAULT_REFINLOW ){
        		flTempData[ADCD_eWater] = -273.16f;
        	}else if( (u8error & ADCD_FAULT_REFINHIGH) == ADCD_FAULT_REFINHIGH ){
        		flTempData[ADCD_eWater] = -273.16f;
        	}else if( (u8error & ADCD_FAULT_RTDINLOW) == ADCD_FAULT_RTDINLOW ){
        		flTempData[ADCD_eWater] = -273.16f;
        	}else if( (u8error & ADCD_FAULT_OVUV) == ADCD_FAULT_OVUV ){
        		flTempData[ADCD_eWater] = -273.16f;
        	} else {
        		flTempData[ADCD_eWater] = -273.16f;
        	}

        	if(boFatal) ERRH_vSetError(TEMP_ERROR_MASK | TEMP_ERROR_SENSORW_MASK | TEMP_ERROR_SENSOR_FAILURE | u8error);


          /** @todo call error handler temp data */
        } else if( (u8error & ADCD_SPI_FAILURE) == ADCD_SPI_FAILURE ){

        	flTempData[ADCD_eWater] = -273.16f;

        	ERRH_vSetError(TEMP_ERROR_MASK | TEMP_ERROR_SENSORW_MASK | TEMP_ERROR_SPI_FAILURE);
        } else {
        	flTempData[ADCD_eWater] = -273.16f;

        	ERRH_vSetError(TEMP_ERROR_MASK | TEMP_ERROR_SENSORW_MASK | TEMP_ERROR_GENERAL_FAILURE);
        }

        u8error = 0;
        boOK = TRUE;
      }


    boOK &= ADCD_boReadData( ADCD_eModule, &u8error, &u16ADC_data );

    if( boOK )
    {
      flTempData[ADCD_eModule] = flConvertADCData( u16ADC_data );
    } else {
      if( (u8error & ADCD_STATUS_DATA_ERROR) == ADCD_STATUS_DATA_ERROR ){
    	  BOOL boFatal = TRUE;
    	  if( (u8error & ADCD_FAULT_HIGHTHRESH) == ADCD_FAULT_HIGHTHRESH ){ // open
    		  flTempData[ADCD_eModule] = 4200.0f;
    		  boFatal &= FALSE;
    	  }else if( (u8error & ADCD_FAULT_LOWTHRESH) == ADCD_FAULT_LOWTHRESH ){ // shorted
    		  flTempData[ADCD_eModule] = -273.16f;
    		  boFatal &= FALSE;
    	  }else if( (u8error & ADCD_FAULT_REFINLOW) == ADCD_FAULT_REFINLOW ){
    		  flTempData[ADCD_eModule] = -273.16f;
    	  }else if( (u8error & ADCD_FAULT_REFINHIGH) == ADCD_FAULT_REFINHIGH ){
    		  flTempData[ADCD_eModule] = -273.16f;
    	  }else if( (u8error & ADCD_FAULT_RTDINLOW) == ADCD_FAULT_RTDINLOW ){
    		  flTempData[ADCD_eModule] = -273.16f;
    	  }else if( (u8error & ADCD_FAULT_OVUV) == ADCD_FAULT_OVUV ){
    		  flTempData[ADCD_eModule] = -273.16f;
    	  } else {
    		  flTempData[ADCD_eModule] = -273.16f;
    	  }

    	  if(boFatal) ERRH_vSetError(TEMP_ERROR_MASK | TEMP_ERROR_SENSORM_MASK | TEMP_ERROR_SENSOR_FAILURE | u8error);
        
      } else if( (u8error & ADCD_SPI_FAILURE) == ADCD_SPI_FAILURE ){
        
    	  flTempData[ADCD_eModule] = -273.16f;

    	  ERRH_vSetError(TEMP_ERROR_MASK | TEMP_ERROR_SENSORM_MASK | TEMP_ERROR_SPI_FAILURE);

      } else {
    	  flTempData[ADCD_eModule] = -273.16f;
    	  ERRH_vSetError(TEMP_ERROR_MASK | TEMP_ERROR_SENSORM_MASK | TEMP_ERROR_GENERAL_FAILURE);
      }



      u8error = 0;
      boOK = TRUE;
    }
    
    VARH_vSetVariableDataFromSystemFloat( VARH_eTemp_Water, flTempData[ADCD_eWater] );
    VARH_vSetVariableDataFromSystemFloat( VARH_eTemp_Module, flTempData[ADCD_eModule] );
//    VARH_vSetVariableDataFromSystemFloat( VARH_eTemp_Diff, flTempData[ADCD_eWater] - flTempData[ADCD_eModule] );

    
    osDelay(REFRESH_MS);
  }
}

//-------------------------------------------------------------------------------------------------
// Function:      flConvertADCData
// Description:   Converts resistor value(PT1000) to temperature data
// Parameters:    U16 u16RTemp
// Returns:       U16, temperature in Celcius
//-------------------------------------------------------------------------------------------------
PRIVATE FLOAT flConvertADCData( U16 u16RTemp )
{

  FLOAT u16R = u16RTemp / 8151.24f;

  FLOAT flT = 9.9714f * u16R;
  flT += 235.904f;
  flT *= u16R;
  flT += -245.876f;

  return( flT );
}
