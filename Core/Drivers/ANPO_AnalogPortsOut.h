//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V2
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        Analog Ports Output
// Filename:      ANPO_AnalogPortsOut.h
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------

#ifndef ANPO_ANALOGPORTSOUT_H
#define ANPO_ANALOGPORTSOUT_H

#ifdef __cplusplus
extern "C" {
#endif

//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files (visible by all modules).
//=================================================================================================

#include "../SDEF_StandardDefinitions.h"

//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       MACROS
// Description:   Definition of global macros (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of global enumerations (visible by all modules).
//=================================================================================================

typedef enum
{
  ANPO_eControlVoltage        = 0,      // 00 control Voltage
 
  ANPO_eInNumberOfInputs,               // Must be last entry     
} ANPO_EnAnalogOutput;


//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of global Structures (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL VARIABLES
// Description:   Definition of global variables (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL CONSTANTS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of global functions (visible by all modules).
//=================================================================================================

BOOL ANPO_boInitializeModule( VOID );
BOOL ANPO_boSetVoltage( FLOAT flVoltage );

#ifdef __cplusplus
}
#endif

#endif
