//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V2
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        User Flash
// Filename:      USFL_UserFlash.c
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------
//
// Description:   This source file contains all functions dealing with the virtual eeprom
//
// More info in the AN4894 or this link:
// https://www.st.com/en/embedded-software/x-cube-eeprom.html
//
//=================================================================================================



//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files.
//=================================================================================================

#include "USFL_UserFlash.h"
#include "UTIL_Utility.h"

// include STM32 drivers
#include "stm32l4xx_hal.h"

#include "cmsis_os2.h"

#include "eeprom_emul.h"

//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================



//=================================================================================================
// Section:       MACROS
// Description:   Definition of local macros (visible by this module only).
//=================================================================================================



//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of local enumerations (visible by this module only).
//=================================================================================================



//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of local Structures (visible by this module only).
//=================================================================================================
 


//=================================================================================================
// Section:       LOCAL VARIABLES
// Description:   Definition of local variables (visible by this module only).
//=================================================================================================

LOCAL osMutexId_t         m_pstMutexID        = NULL;

//=================================================================================================
// Section:       LOCAL CONSTANTS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================

LOCAL CONST osMutexAttr_t m_stMutexAttr =
{
  "USFL_Mutex",                             // human readable mutex name
  osMutexRecursive | osMutexPrioInherit,    // attr_bits
  NULL,                                     // memory for control block   
  0U                                        // size for control block
};

//=================================================================================================
// Section:       LOCAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of local functions (visible by this module only).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL FUNCTIONS
// Description:   Definition (implementation) of global functions.
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// Function:      USFL_boInitializeModule
// Description:   Initializes the module. Function must be called once immediately after power-up.
// Parameters:    None
// Returns:       Boolean TRUE if successful
//-------------------------------------------------------------------------------------------------
BOOL USFL_boInitializeModule( VOID )
{
  BOOL boOK = TRUE;

  boOK &= ( ( m_pstMutexID = osMutexNew( &m_stMutexAttr )) == NULL) ? FALSE : TRUE;

  boOK &= USFL_vUnlock();
  boOK &= EE_Init(EE_FORCED_ERASE) == EE_OK ? TRUE : FALSE;
  boOK &= USFL_vLock();

  return( boOK );
}

//-------------------------------------------------------------------------------------------------
// Function:      USFL_boGetVariable
// Description:   Gets a variable out of the virtual eeprom
// Parameters:    U8 u8Variable -> virtual address of variable
//                U32 * u32Variable -> pointer to data
// Returns:       Boolean TRUE if successful
//-------------------------------------------------------------------------------------------------
BOOL USFL_boGetVariable ( U8 u8Variable, VARH_UVariable * uVariable ){
  BOOL boOK = TRUE;

  boOK &= EE_ReadVariable32bits( (U16) u8Variable, (PU32)uVariable ) == EE_OK ? TRUE : FALSE;

  return boOK;
}

//-------------------------------------------------------------------------------------------------
// Function:      USFL_boSetVariable
// Description:   Writes a variable into the virtual eeprom
// Parameters:    U8 u8Variable -> virtual address of variable
//                U32 u32Variable -> data to write
// Returns:       Boolean TRUE if successful
//-------------------------------------------------------------------------------------------------
BOOL USFL_boSetVariable ( U8 u8Variable, VARH_UVariable uVariable ){
  BOOL boOK = TRUE;
  EE_Status ee_status = EE_OK;

  ee_status = EE_WriteVariable32bits( (U16) u8Variable, uVariable.u32Val );

  /* Start cleanup mode, if cleanup is needed */
  if ((ee_status & EE_STATUSMASK_CLEANUP) == EE_STATUSMASK_CLEANUP) {
    boOK &= (ee_status |= EE_CleanUp()) == EE_OK ? TRUE : FALSE;

    boOK &= EE_WriteVariable32bits( (U16) u8Variable, uVariable.u32Val ) == EE_OK ? TRUE : FALSE;
  }
  if ((ee_status & EE_STATUSMASK_ERROR) == EE_STATUSMASK_ERROR) {boOK &= FALSE;}

  return boOK;
}

//-------------------------------------------------------------------------------------------------
// Function:      USFL_vLock
// Description:   Locks the Flash, no more clearing and programming possible
// Parameters:    None
// Returns:       Boolean TRUE if successful
//-------------------------------------------------------------------------------------------------
BOOL USFL_vLock( VOID ){
  BOOL boOK = TRUE;

  boOK &= HAL_FLASH_Lock() == HAL_OK ? TRUE : FALSE;
  osMutexRelease( m_pstMutexID );        											// release mutex

  return boOK;
}

//-------------------------------------------------------------------------------------------------
// Function:      USFL_vUnlock
// Description:   Unlocks the Flash, clearing and programming possible
// Parameters:    None
// Returns:       Boolean TRUE if successful
//-------------------------------------------------------------------------------------------------
BOOL USFL_vUnlock( VOID ){
  BOOL boOK = TRUE;

  osMutexAcquire( m_pstMutexID, osWaitForever );                                  // aquire mutex
  boOK &= HAL_FLASH_Unlock() == HAL_OK ? TRUE : FALSE;

  return boOK;
}


//=================================================================================================
// Section:       LOCAL FUNCTIONS
// Descriptionn:  Definition (implementation) of local functions.
//=================================================================================================

