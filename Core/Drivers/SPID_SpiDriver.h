//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V2
// Author:        Noah Piqu� (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        SPI-Driver
// Filename:      SPID_SpiDriver.h
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------

#ifndef SPID_SPIDRIVER_H
#define SPID_SPIDRIVER_H

#ifdef __cplusplus
extern "C" {
#endif



//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files (visible by all modules).
//=================================================================================================

#include "../SDEF_StandardDefinitions.h"

#include "../Drivers/DIPO_DigitalPorts.h"

// include STM32 drivers
#include "stm32l4xx_hal.h"
  
//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       MACROS
// Description:   Definition of global macros (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of global enumerations (visible by all modules).
//=================================================================================================

// do not change the order of the enumeration!
typedef enum
{
  SPID_eADC    = 0,
  
  SPID_eNumberOfSPIs,  // Must be last
} SPID_EnSPIs;



//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of global Structures (visible by all modules).
//=================================================================================================

typedef struct
{
  SPID_EnSPIs           enSPI;
  DIPO_EnDigitalOutput  enCS;
  PU8                   pu8TxBuf;
  PU8                   pu8RxBuf;
  U16                   u16TransferSize;
  SPI_InitTypeDef       stInit;
} SPID_StHandle;

//=================================================================================================
// Section:       GLOBAL VARIABLES
// Description:   Definition of global variables (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL CONSTANTS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of global functions (visible by all modules).
//=================================================================================================
BOOL  SPID_boInitializeModule( VOID );
BOOL  SPID_boSend( SPID_StHandle* pstHandle );
BOOL  SPID_boSendReceive( SPID_StHandle* pstHandle );

#ifdef __cplusplus
}
#endif

#endif
