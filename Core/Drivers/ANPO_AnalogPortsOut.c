//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V2
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        Analog Ports Output
// Filename:      ANPO_AnalogPortsOut.c
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------
//
// Description:   This source file contains all functions dealing with the analog outputs ports
//
//=================================================================================================

//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files.
//=================================================================================================

#include "ANPO_AnalogPortsOut.h"
#include "ANPI_AnalogPortsIn.h"

#include "VARH_VariableHandler.h"
#include "DIPO_DigitalPorts.h"

// Toolbox
#include "../Toolbox/UTIL_Utility.h"

// include STM32 drivers
#include "stm32l4xx_hal.h"

#include "cmsis_os2.h"

//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================

#define REFRESH 50 // Refresh rate in ms

#define EVENT_TIMER_UPDATE ((U32)(1 << 0))

#define EVENT_FLAGS_ALL (EVENT_TIMER_UPDATE)

#define OFFSET 20.088
#define FACTOR 34.103448

//=================================================================================================
// Section:       MACROS
// Description:   Definition of local macros (visible by this module only).
//=================================================================================================

//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of local enumerations (visible by this module only).
//=================================================================================================

//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of local Structures (visible by this module only).
//=================================================================================================

//=================================================================================================
// Section:       LOCAL VARIABLES
// Description:   Definition of local variables (visible by this module only).
//=================================================================================================

LOCAL osThreadId_t m_pstThreadID = NULL;
LOCAL osTimerId_t m_pstUpdateTimer = NULL;
LOCAL osEventFlagsId_t m_pstEventID = NULL;

LOCAL FLOAT flVoltageActual = 0.0f;
LOCAL FLOAT flVoltageSet = 0.0f;

//=================================================================================================
// Section:       LOCAL CONSTANTS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================

LOCAL CONST osThreadAttr_t stTaskAttribute =
    {
        "ANPO_Thread",    // name of the thread
        osThreadDetached, // attribute bits
        NULL,             // memory for control block
        0,                // size of provided memory for control block
        NULL,             // memory for stack
        1024,             // size of stack
        osPriorityNormal, // initial thread priority (default: osPriorityNormal)
        0,                // TrustZone module identifier
        0,                // reserved (must be 0)
};

LOCAL CONST osTimerAttr_t stTimerAttribute =
    {
        "ANPO_UpdateTimer", // name of the timer
        0,                  // attribute bits
        NULL,               // memory for control block
        0,                  // size of provided memory for control block
};

LOCAL CONST osEventFlagsAttr_t stEventAttribute =
    {
        "ANPO_Event_Flags", // name of the event flags
        0,                  // attribute bits
        NULL,               // memory for control block
        0,                  // size of provided memory for control block
};

//=================================================================================================
// Section:       LOCAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of local functions (visible by this module only).
//=================================================================================================

U32 u32ConvertVoltagetoRaw(FLOAT flVoltage);
BOOL boUpdateVoltage(VOID);

PRIVATE VOID vTask(PVOID arg);
PRIVATE VOID vEventCallback(PVOID pvData);

//=================================================================================================
// Section:       EXTERNAL FUNCTIONS
// Description:   Definition of external (global) functions.
//=================================================================================================

//=================================================================================================
// Section:       EXTERNAL VARIABLES
// Description:   Definition of external (global) variables.
//=================================================================================================

extern DAC_HandleTypeDef hdac1;

//=================================================================================================
// Section:       GLOBAL FUNCTIONS
// Description:   Definition (implementation) of global functions.
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// Function:      ANPO_boInitializeModule
// Description:   Initializes the module. Function must be called once immediately after power-up.
// Parameters:    None
// Returns:       Boolean, TRUE if successful
//-------------------------------------------------------------------------------------------------

BOOL ANPO_boInitializeModule(VOID)
{
  BOOL boOK = TRUE;

  boOK &= ((m_pstThreadID = osThreadNew(vTask, NULL, &stTaskAttribute)) == NULL) ? FALSE : TRUE;
  boOK &= ((m_pstEventID = osEventFlagsNew(&stEventAttribute)) == NULL) ? FALSE : TRUE;
  boOK &= (m_pstUpdateTimer = osTimerNew(vEventCallback, osTimerPeriodic, (PVOID)EVENT_TIMER_UPDATE, &stTimerAttribute)) == NULL ? FALSE : TRUE;

  boOK &= HAL_DAC_Start(&hdac1, DAC_CHANNEL_1) == HAL_OK ? TRUE : FALSE;

  boOK &= (osTimerStart(m_pstUpdateTimer, REFRESH) == osOK) ? TRUE : FALSE;

  return (boOK);
}

//=================================================================================================
// Section:       LOCAL FUNCTIONS
// Descriptionn:  Definition (implementation) of local functions.
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// Function:      vTask
// Description:   Task
// Parameters:    None
// Returns:       None
//-------------------------------------------------------------------------------------------------
VOID vTask(PVOID arg)
{
  UNUSED(arg);
  U32 u32EventFlags = 0;

  while (TRUE)
  {
    u32EventFlags = osEventFlagsWait(m_pstEventID, EVENT_FLAGS_ALL, osFlagsWaitAny, osWaitForever);

    if (u32EventFlags & EVENT_TIMER_UPDATE)
    {
      boUpdateVoltage();

      BOOL boPowerGood = DIPO_boGetInput(DIPO_ePG);
      /** @todo check power good pin */
    }
  }
}

//-------------------------------------------------------------------------------------------------
// Function:      ANPO_boSetVoltage
// Description:   Sets the Variable to a specific Voltage
// Parameters:    FLOAT Voltage
// Returns:       Boolean, TRUE if successful
//-------------------------------------------------------------------------------------------------
BOOL ANPO_boSetVoltage(FLOAT flVoltage)
{
  BOOL boOK = TRUE;

  flVoltageSet = flVoltage;

  boOK &= osEventFlagsSet(m_pstEventID, EVENT_TIMER_UPDATE) == osOK ? TRUE : FALSE;


  return (boOK);
}

//-------------------------------------------------------------------------------------------------
// Function:      boUpdateVoltage
// Description:   Updates the Voltage of the DAC Output with a ramp
// Parameters:    FLOAT Voltage
// Returns:       Boolean, TRUE if successful
//-------------------------------------------------------------------------------------------------
BOOL boUpdateVoltage(VOID)
{
  BOOL boOK = TRUE;


  if (flVoltageActual < flVoltageSet)
  {
	flVoltageActual += 0.1;
	if (flVoltageActual > flVoltageSet)
	{
	  flVoltageActual = flVoltageSet;
	}
  }
  else if (flVoltageActual > flVoltageSet)
  {
	flVoltageActual -= 0.1;
	if (flVoltageActual < flVoltageSet)
	{
	  flVoltageActual = flVoltageSet;
	}
  }

  U32 u32RawData = u32ConvertVoltagetoRaw(flVoltageActual);

  boOK &= HAL_DAC_SetValue(&hdac1, DAC_CHANNEL_1, DAC_ALIGN_12B_R, u32RawData) == HAL_OK ? TRUE : FALSE;

  return (boOK);
}

//-------------------------------------------------------------------------------------------------
// Function:      u32ConvertVoltagetoRaw
// Description:   Convert Voltage to Raw value for the DAC
// Parameters:    FLOAT Voltage
// Returns:       U32
//-------------------------------------------------------------------------------------------------
U32 u32ConvertVoltagetoRaw(FLOAT flVoltage)
{
  U32 RawData;

  FLOAT flTempVoltage = (((FLOAT)flVoltage) + OFFSET) / FACTOR;

  RawData = flTempVoltage * 4095 / VARH_flGetVariableData(VARH_eRef_U);

  return RawData;
}

//-------------------------------------------------------------------------------------------------
// Function:      vEventCallback
// Description:   Callback for events
// Parameters:    None
// Returns:       None
//-------------------------------------------------------------------------------------------------
PRIVATE VOID vEventCallback(PVOID pvData)
{
  osEventFlagsSet(m_pstEventID, (U32)pvData);
}
