//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       Peltier Controller V2
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        Analog Ports Input
// Filename:      ANPI_AnalogPortsIn.h
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------

#ifndef ANPI_ANALOGPORTSIN_H
#define ANPI_ANALOGPORTSIN_H

#ifdef __cplusplus
extern "C" {
#endif



//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files (visible by all modules).
//=================================================================================================

#include "../SDEF_StandardDefinitions.h"

//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================


//=================================================================================================
// Section:       MACROS
// Description:   Definition of global macros (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of global enumerations (visible by all modules).
//=================================================================================================

// Enumeration for analog inputs is used as array index later on. Thus enumeration must start 
// at zero and must be numbered consecutively. Enumeration must never be changed.

typedef enum
{
  ANPI_eSupplyVoltage24V    	= 0,      // voltage of 24V power supply
  ANPI_eSupplyCurrent24V   		= 1,      // current of 24V power supply
  ANPI_eOutputVoltage      		= 2,      // output voltage peltier element
  ANPI_eOutputCurrent         = 3,      // output current peltier element
  
  ANPI_eInNumberOfInputs,               // Must be last entry     
} ANPI_EnAnalogInput;


//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of global Structures (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL VARIABLES
// Description:   Definition of global variables (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL CONSTANTS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of global functions (visible by all modules).
//=================================================================================================

BOOL ANPI_boInitializeModule( VOID );
FLOAT ANPI_GetVoltage( ANPI_EnAnalogInput enVoltage );

#ifdef __cplusplus
}
#endif

#endif
